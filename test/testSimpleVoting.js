/*
 * 1. Run ganache-cli as `ganache-cli -m "candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"`
 * to ensure same adresses on each startup
 * 2. Run `truffle test`
 */

const SimpleVoting = artifacts.require("./SimpleVoting.sol");
var testSuite = require("../sample/TestSuites.json").suites[0].testData;

contract('SimpleVoting', function(accounts) {
	it("The voting administrator can change stage to VOTING", async function() {
	  let simpleVotingInstance = await SimpleVoting.deployed()
	  let votingAdministrator = await simpleVotingInstance.administrator()

	  let result = await simpleVotingInstance.startVotingSession({from: votingAdministrator})

      assertStageSwitch(result, Stages.VOTING)
	});

    it("The voter can vote during voting session", async function() {
      let simpleVotingInstance = await SimpleVoting.deployed();

      let result = await simpleVotingInstance.vote(testSuite.votes[0], testSuite.certificates[0],
          testSuite.voteProofs[0], {from: accounts[0]})

      assertVotedEvent(result, accounts[0])
    });

    it("The voting administrator can change stage to TALLYING", async function() {
	  let simpleVotingInstance = await SimpleVoting.deployed()
	  let votingAdministrator = await simpleVotingInstance.administrator()

	  let result = await simpleVotingInstance.endVotingSession({from: votingAdministrator})

      assertStageSwitch(result, Stages.TALLYING)
	});

    it("The authority can add decryption share", async function() {
      let simpleVotingInstance = await SimpleVoting.deployed();

      await simpleVotingInstance.addDecryptionShare(testSuite.decryptionShares[0],
          testSuite.decryptionShareProofs[0], {from: accounts[9]})
      let callResult = await simpleVotingInstance.addDecryptionShare(testSuite.decryptionShares[1],
          testSuite.decryptionShareProofs[1], {from: accounts[8]})

      assertStageSwitch(callResult, Stages.FINISHED)

      let result = await simpleVotingInstance.getResult()
      assert.equal(result.homomorphicSum.a.value, testSuite.finalResult.homomorphicSum.a.value)
      assert.equal(result.homomorphicSum.b.value, testSuite.finalResult.homomorphicSum.b.value)
      assert.deepEqual(result.decryptionShares, testSuite.finalResult.decryptionShares)
    });
});

const Stages = {
    NOT_STARTED: 0,
    VOTING: 1,
    TALLYING: 2,
    FINISHED: 3
}

function assertStageSwitch(result, newStage) {
    assert.equal(result.logs[0].event, 'ActiveStageChangedEvent')
    assert.equal(result.logs[0].args.newStage, newStage)
}

function assertVotedEvent(result, address) {
    assert.equal(result.logs[0].event, 'VotedEvent')
    assert.equal(result.logs[0].args.voter, address)
}
