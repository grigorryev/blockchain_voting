var SimpleVoting = artifacts.require("SimpleVoting");
var BlindSignature = artifacts.require("BlindSignature");
var HomomorphicScheme = artifacts.require("HomomorphicScheme");
var ZeroKnowledgeProof = artifacts.require("ZKP");
var Math = artifacts.require("Math");
var testSuite = require("../sample/TestSuites.json").suites[0].contractParams;

module.exports = function (deployer) {
    deployer.deploy(Math)
    deployer.link(Math, BlindSignature);
    deployer.link(Math, HomomorphicScheme);
    deployer.link(Math, ZeroKnowledgeProof);
    deployer.deploy(BlindSignature)
    deployer.deploy(HomomorphicScheme)
    deployer.deploy(ZeroKnowledgeProof)
    deployer.link(HomomorphicScheme, ZeroKnowledgeProof);
    deployer.link(Math, SimpleVoting);
    deployer.link(BlindSignature, SimpleVoting);
    deployer.link(HomomorphicScheme, SimpleVoting);
    deployer.link(ZeroKnowledgeProof, SimpleVoting);
    // deployer.link(Common, SimpleVoting);
    deployer.deploy(
        SimpleVoting,
        testSuite.blindSignature.publicKeys,
        testSuite.homomorphicScheme.params,
        testSuite.homomorphicScheme.zero,
        testSuite.authorities.count,
        testSuite.authorities.authoritiesAddresses,
        testSuite.authorities.authoritiesPublicKeys,
    );
}
