pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "./BlindSignature.sol";
import "./HomomorphicScheme.sol";
import "./ZeroKnowledgeProof.sol";
import "./Math.sol";

contract SimpleVoting {
    // Стадии голосования
    enum Stage {
        NOT_STARTED,  // не началось
        VOTING,       // голосование идет
        TALLYING,     // подсчет голосов
        FINISHED      // голосование окончено
    }

    // Окончательный результат голосования
    struct Result {
        HomomorphicScheme.Ciphertext homomorphicSum;
        Math.BigInteger[] decryptionShares;
    }
    Result finalResult;

    // Число организаторов
    uint authoritiesCount;
    // Отображение (адрес организатора -> его порядковый номер)
    mapping(address => uint) private authorities;
    // Открытые ключи организаторов для участия в пороговой схеме
    mapping(address => Math.BigInteger) private authoritiesPublicKeys;

    // Ключи проверки подписи для сертификатов голосующих
    BlindSignature.PublicKey[] private blindSignaturePublicKeys;

    // Параметры схемы гомоморфного шифрования
    HomomorphicScheme.SchemeParams private homomorphicSchemeParams;

    // Промежуточный результат голосования
    HomomorphicScheme.Ciphertext public intermediateResult;

    // Счетчик полученных частей открытого текста результата в рамках пороговой схемы
    uint decryptionSharesReceived = 0;
    // Части открытого текста результата
    Math.BigInteger[] decryptionShares;

    // Текущая стадия голосования
    Stage public activeStage;

    // Администратор голосования
    address public administrator;

    // Список проголосовавших
    mapping(address => bool) public alreadyVoted;

    // Конструктор смарт-контракта. Принимает информацию об организаторах,
    // их открытые ключи, а также параметры алгоритмов шифрования
    constructor(
        BlindSignature.PublicKey[] memory _blindSignaturePublicKeys,
        HomomorphicScheme.SchemeParams memory _homomorphicSchemeParams,
        HomomorphicScheme.Ciphertext memory _zero,
        uint _authoritiesCount,
        address[] memory _authoritiesAddresses,
        Math.BigInteger[] memory _authoritiesPublicKeys
    ) public {
        homomorphicSchemeParams = _homomorphicSchemeParams;
        intermediateResult = _zero;
        authoritiesCount = _authoritiesCount;

        for (uint i = 0; i < _authoritiesCount; i++) {
            decryptionShares.push(Math.BigInteger(0));
            authorities[_authoritiesAddresses[i]] = i + 1;
            authoritiesPublicKeys[_authoritiesAddresses[i]] = _authoritiesPublicKeys[i];
            blindSignaturePublicKeys.push(_blindSignaturePublicKeys[i]);
        }
        decryptionShares.push(Math.BigInteger(0));

        administrator = msg.sender;
        activeStage = Stage.NOT_STARTED;
    }

    // Начинает стадию голосования
    function startVotingSession() onlyDuring(Stage.NOT_STARTED) public onlyAdministrator {
        activeStage = Stage.VOTING;
        emit ActiveStageChangedEvent(activeStage);
    }

    // Заканчисвает стадию голосования
    function endVotingSession() onlyDuring(Stage.VOTING) public onlyAdministrator {
        activeStage = Stage.TALLYING;
        emit ActiveStageChangedEvent(activeStage);
    }

    // Метод, используемый голосующими для подачи голоса
    function vote(HomomorphicScheme.Ciphertext memory theVote, BlindSignature.Certificate memory certificate,
        ZKP.VoteProof memory voteProof) onlyDuring(Stage.VOTING) public {

        // 1. Проверить, что пользователь не голосовал ранее
        require(!alreadyVoted[msg.sender], "Caller has already voted");

        // 2. Проверить подлинность подписей организаторов, допустивших голосующего к голосованию
        require(BlindSignature.checkCertificate(certificate, blindSignaturePublicKeys, msg.sender), "Invalid certificate");

        // 3. Проверить доказательство корректности голоса
        require(ZKP.checkVoteProof(voteProof, homomorphicSchemeParams, theVote), "Invalid vote proof");

        // 4. Учесть голос в результирующей гомоморфной сумме
        intermediateResult = HomomorphicScheme.homomorphicAdd(intermediateResult, theVote, homomorphicSchemeParams);

        // 5. Занести голосующего в список проголосовавших
        alreadyVoted[msg.sender] = true;

        // 6. Опубликовать событие
        emit VotedEvent(msg.sender);
    }

    // Метод, позволяющий организатору опубликовать свою часть отрытого текста в
    // рамках пороговой схемы. Может быть вызван только организатором, и только после
    // перехода голосования в стадию TALLYING. После успешного вызова этого метода
    // всеми организаторами, голосование автоматически перейдет в состояние FINISHED
    function addDecryptionShare(Math.BigInteger memory share, ZKP.ChaumPedersenProof memory shareProof)
        onlyAuthority onlyDuring(Stage.TALLYING) public {
        // 1. Проверить доказательство корректного расшифрования
        require(ZKP.checkDecryptionShareProof(intermediateResult, share, shareProof,
            authoritiesPublicKeys[msg.sender], homomorphicSchemeParams), "Invalid decryption share proof");

        // 2. Сохранить полученную часть открытого текста
        uint shareIndex = authorities[msg.sender];
        if (decryptionShares[shareIndex].value == 0) {
            decryptionSharesReceived += 1;
            decryptionShares[shareIndex] = share;
        }

        // 3. Если получены части открытого текста от всех организаторов -
        // перевести голосование в состояние FINISHED и сохранить результат
        if (decryptionSharesReceived == authoritiesCount) {
            finalResult.homomorphicSum = intermediateResult;
            for (uint i = 0; i < decryptionSharesReceived; i++) {
                finalResult.decryptionShares.push(decryptionShares[i+1]);
            }

            activeStage = Stage.FINISHED;
            emit ActiveStageChangedEvent(activeStage);
        }
    }

    function getResult() public view returns (Result memory) {
        return finalResult;
    }

    // Позволяет вызвать метод только администратору
    modifier onlyAdministrator() {
        require(msg.sender == administrator, "Method can be called by administrator only");
        _;
    }

    // Позволяет вызвать метод только организатору голосования
    modifier onlyAuthority() {
        require(authorities[msg.sender] != 0, "Method can be called by authority only");
        _;
    }

    // Позволяет вызвать метод только организатору голосования
    modifier onlyDuring(Stage stage) {
        require(activeStage == stage, "The voting is in the wrong stage");
        _;
    }

	event VotedEvent (
	    address voter
	);

	event ActiveStageChangedEvent (
        Stage newStage
    );
}
