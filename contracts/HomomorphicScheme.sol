pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "./Math.sol";

library HomomorphicScheme {
    struct SchemeParams {
        Math.BigInteger p;
        Math.BigInteger q;
        Math.BigInteger g;
        Math.BigInteger antiG; // элемент, обратный к g
        Math.BigInteger pk;    // открытый ключ
    }

    struct Ciphertext {
        Math.BigInteger a;
        Math.BigInteger b;
    }

    function homomorphicAdd(Ciphertext memory c1, Ciphertext memory c2, SchemeParams memory params)
        public pure returns (Ciphertext memory) {
        return Ciphertext(Math.mulMod(c1.a, c2.a, params.p), Math.mulMod(c1.b, c2.b, params.p));
    }
}
