pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

library Math {
    struct BigInteger {
        // todo: use real big arithmetics
        uint256 value;
    }

    function powerMod(BigInteger memory x, BigInteger memory y, BigInteger memory p) public pure returns (BigInteger memory) {
        BigInteger memory res = BigInteger(1);
        x.value = x.value % p.value;
        while (y.value > 0)
        {
            if (y.value & 1 != 0)
                res.value = (res.value * x.value) % p.value;
            y.value = y.value >> 1;
            x.value = (x.value * x.value) % p.value;
        }
        return res;
    }

    function mulMod(BigInteger memory x, BigInteger memory y, BigInteger memory p)
        public pure returns (Math.BigInteger memory) {
        return BigInteger(mulmod(x.value, y.value, p.value));
    }

    function sum(BigInteger memory x, BigInteger memory y)
        public pure returns (Math.BigInteger memory) {
        return BigInteger(x.value + y.value);
    }

    function mod(BigInteger memory x, BigInteger memory y)
        public pure returns (Math.BigInteger memory) {
        return BigInteger(x.value % y.value);
    }

    function equals(BigInteger memory x, BigInteger memory y)
        public pure returns (bool) {
        return x.value == y.value;
    }

    function toBigInteger(address addr) public pure returns (BigInteger memory) {
        return BigInteger(uint256(addr));
    }
}
