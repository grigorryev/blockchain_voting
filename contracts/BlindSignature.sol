pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "./Math.sol";

library BlindSignature {

    struct PublicKey {
        Math.BigInteger e;
        Math.BigInteger N;
    }

    struct Certificate {
        Math.BigInteger[] signatures;
    }

    function checkCertificate(Certificate memory certificate, PublicKey[] storage publicKeys, address voterAddress)
        public view returns (bool) {

        Math.BigInteger memory addressAsNumber = Math.toBigInteger(voterAddress);
        for (uint i = 0; i < publicKeys.length; i++) {
            if (!Math.equals(
                Math.powerMod(certificate.signatures[i], publicKeys[i].e, publicKeys[i].N),
                Math.mod(addressAsNumber, publicKeys[i].N)
            )) {
                return false;
            }
        }
        return true;
    }
}
