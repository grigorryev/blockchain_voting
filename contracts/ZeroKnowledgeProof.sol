pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "./HomomorphicScheme.sol";
import "./Math.sol";

library ZKP {

    struct ChaumPedersenProof {
        Math.BigInteger u;
        Math.BigInteger v;
        Math.BigInteger R;
        Math.BigInteger c;
    }

    struct VoteProof {
        ChaumPedersenProof first;
        ChaumPedersenProof second;
        Math.BigInteger c;
    }

    function checkVoteProof(VoteProof memory proof, HomomorphicScheme.SchemeParams storage params,
        HomomorphicScheme.Ciphertext memory vote) public view returns (bool) {
        Math.BigInteger memory c = getChallenge(proof, vote, params);
        if (!Math.equals(Math.sum(proof.first.c, proof.second.c), c))
            return false;

        return checkChaumPedersonProof(proof.first, params, vote, Math.BigInteger(1))
            && checkChaumPedersonProof(proof.second, params, vote, params.antiG);
    }

    function checkDecryptionShareProof(HomomorphicScheme.Ciphertext memory intermediateResult,
        Math.BigInteger memory decryptionShare, ChaumPedersenProof memory proof,
        Math.BigInteger memory publicKey, HomomorphicScheme.SchemeParams storage params)
        public view returns (bool) {
        /* if (!Math.equals(
                Math.BigInteger(sha256(publicKey, intermediateResult.a, intermediateResult.b, proof.u, proof.v)),
                proof.c)) {
            return false;
        } */

        return Math.equals(
            Math.powerMod(intermediateResult.a, proof.R, params.p),
            Math.mulMod(proof.u, Math.powerMod(decryptionShare, proof.c, params.p), params.p)
        ) && Math.equals(
            Math.powerMod(params.g, proof.R, params.p),
            Math.mulMod(proof.v, Math.powerMod(publicKey, proof.c, params.p), params.p)
        );
    }

    function checkChaumPedersonProof(ChaumPedersenProof memory proof,
        HomomorphicScheme.SchemeParams storage params, HomomorphicScheme.Ciphertext memory ciphertext,
        Math.BigInteger memory bMultiplier) public view returns (bool) {
        Math.BigInteger memory b1 = Math.mulMod(ciphertext.b, bMultiplier, params.p);
        return Math.equals(
            Math.powerMod(params.g, proof.R, params.p),
            Math.mulMod(proof.u, Math.powerMod(ciphertext.a, proof.c, params.p), params.p)
        ) && Math.equals(
            Math.powerMod(params.pk, proof.R, params.p),
            Math.mulMod(proof.v, Math.powerMod(b1, proof.c, params.p), params.p)
        );
    }

    function getChallenge(VoteProof memory proof, HomomorphicScheme.Ciphertext memory vote,
        HomomorphicScheme.SchemeParams storage params)
        private pure returns (Math.BigInteger memory) {
        //return Math.BigInteger(sha256(proof.first.u, proof.first.v, proof.second.u, proof.second.v, params.pk, vote.a, vote.b))
        return Math.BigInteger(579);
    }
}
